#!/bin/bash
# Shell script to set up the app.
# This script should be called from inside the app container,
# because it makes use of the container's environment variables
# and installed python dependencies.

# Wipe the database
echo "Removing database, if present..."
rm db.sqlite3
# Prepare a new database
python manage.py makemigrations
python manage.py migrate
# Create a root user
python manage.py slp_createsuperuser
# Publish shacl shapes and save asset IDs in the database
python manage.py set_shacl
echo "Finished application setup."