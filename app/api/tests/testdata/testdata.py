# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""Data for testing purposes.

The data is structured as a nested dictionary, 
nesting first by object type,
and within each type, by validity.
For valid and invalid cases, respectively,
the deepest dictionaries hold titles of testcases
as keys, and the data itself as values.
"""

import json
import os
from django.conf import settings

# Read in data files
testdata_dir = os.path.join(
  settings.BASE_DIR,
  'api/tests/testdata'
)

# Acts 
with open(os.path.join(testdata_dir, 'aanvraag_rvo.json'), 'r') as json_file:
  rvo = json.loads(json_file.read())
# Facts
with open(os.path.join(testdata_dir, 'vreemdeling_fact.json'), 'r') as json_file:
  vreemdeling = json.loads(json_file.read())
# Duties
with open(os.path.join(testdata_dir, 'geldigheidsduur_duty.json'), 'r') as json_file:
  geldigheidsduur_duty = json.loads(json_file.read())
# Models
with open(os.path.join(testdata_dir, '2x2x2_model.json'), 'r') as json_file:
  tiny_model = json.loads(json_file.read())


# Define encompassing dict
data = {
  "models": {
    "valid":{
      "Vreemdelingen tiny (2x2x2)": tiny_model,
    }
  },
  "acts": {
    "valid": {
      "Aanvraag RVO": rvo,
    }
  },
  "facts": {
    "valid": {
      "Vreemdeling": vreemdeling,
    }
  },
  "duties": {
    "valid": {
      "Geldigheidsduur": geldigheidsduur_duty,
    }
  }
}
