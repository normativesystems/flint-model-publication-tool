# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from .calculemus_basic_test_cases import CalculemusBasicTestCase

class TestFacts(CalculemusBasicTestCase):
    """
    Test post, single/list retrieval of Facts.
    """
    label = "models"

    def test(self):
        # Careful with the method naming,
        # else the super class's method gets recognized
        # as a test of its own.
        return super()._test()
        