# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""
#project-specific

Author: Jeroen Breteler
Author affiliation: TNO
Date: 2020-02-07

Module defining the Calculemus Test Case object.
"""

from .SLPTestCase import SLPTestCase
from .testdata import testdata

# To indicate urls for posting objects
from django.urls import reverse

class CalculemusTestCase(SLPTestCase):
    """
    Concrete TestCase class for the Calculemus project.

    Defines test user roles and sets up helper
    functions to post data objects.
    """


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Set user roles
        self.roles = [
            'Robert'
        ]

    def post_object(self, url, data):
        """
        Wrapped function for SLPTestCase.post_object adding user auth
        """
        return super().post_object(url, data, authorizedUser="Robert")

    def post_act(self, data=None):
        """
        Post an Act frame
        """
        # Set default data
        if not data:
            title, data = testdata.data['act']['valid'].items()[0]
            print("Using testcase {}".format(title))

        # Pass to the abstract method
        act_asset_id = self.post_object(
            url=reverse('acts_list'),
            data=data,
            authorizedUser='Robert'
        )
        return act_asset_id

