# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from django.http import HttpResponse


def index(request):
    return HttpResponse('Welcome to the Full Truck Load API')

