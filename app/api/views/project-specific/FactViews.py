# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from .SLPView import SLPView, SLPListView, SLPDetailView

class FactView(SLPView):
    def asset_name(self):
        return "Fact"

class FactListView(FactView, SLPListView):
    pass

class FactDetailView(FactView, SLPDetailView):
    pass