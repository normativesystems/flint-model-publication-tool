# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""
A module that defines and handles logic checks that are commonly used.
Methods here will typically be called from views.
"""

import api.slp_util as slp_util
import api.semantics as semantics

from rest_framework.response import Response
from rest_framework import status as http_status

#Reload pls
def checkLogic(listOfChecks):
    """
    Given a list of conditions and relevant arguments,
    this function runs through all conditions and checks whether
    the logic is satisfied.

    If a logic check fails, the method returns a Response, 
    (typically with an error message),
    that can be used by a view.
    Hence, the return variable is called @response.

    If all checks pass, the view does not need to send a Response
    so the method will return None.
    """
    for condition, args in listOfChecks:
        response = condition(*args)
        if response:
            # Immediately return the response, since we
            # only send one response anyway.
            return response    
    return None

# NOTE: All methods return None by default. 
# https://stackoverflow.com/questions/15300550/return-return-none-and-no-return-at-all#15300671

def asset_exists(asset_id):
    """
    Given an asset ID, confirm that the asset exists on the ledger.
    """
    try:
        slp_util.get_asset(asset_id)
    except Exception as e:
        return Response(str(e), status=http_status.HTTP_400_BAD_REQUEST)


def asset_has_type(asset_id, semantic_type):
    """
    Given an asset ID and a semantic type, confirm
    that the asset's rdf contains a subject of the given type.
    """
    try:
        asset = slp_util.get_asset(asset_id)
        if not semantics.has_type(semantic_type, asset):
            return Response('Asset is not of type {}'.format(str(semantic_type)), status=http_status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response(str(e), status=http_status.HTTP_400_BAD_REQUEST)

def asset_owned_by(asset_id, user):
    """
    Given an asset ID and a user account,
    confirm that the user owns this asset on the ledger.
    """
    if not slp_util.owns(user, asset_id):
        return Response('User does not own asset', status=http_status.HTTP_400_BAD_REQUEST)

def asset_is_valid(asset_id):
    """Run the asset through SHACL validation"""
    validation_response = slp_util.validate(asset_id)
    if validation_response != True:
        return Response("Asset did not pass SHACL validation. Failing shapes: {}".format(validation_response),
            status=http_status.HTTP_400_BAD_REQUEST)